import decorators.GreetingsEmailDecorator;
import decorators.HeaderEmailDecorator;
import decorators.SignatureEmailDecorator;
import models.Email;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Main Mailer class
 */
public class Mailer {

    //main title
    private static final String PROGRAM_TITLE = "****************************************" +
            "\nProsty program pocztowy" +
            "\nWciśnij odpowiedni przycisk..." +
            "\n****************************************";

    //available options titles
    private static final String SIMPLE_EMAIL_OPTION = "1. Sam tekst wiadomości";
    private static final String EMAIL_HEADER_OPTION = "2. Wiadomość z nagłówkiem";
    private static final String EMAIL_HEADER_GREETINGS_OPTION = "3. Wiadomość z nagłówkiem oraz pozdrowieniami";
    private static final String EMAIL_HEADER_GREETINGS_SIGNATURE_OPTION = "4. Wiadomość z nagłówkiem, pozdrowieniami oraz podpisem";
    private static final String QUIT_OPTION = "0. Zamknij program";

    //option type values
    private static final int SIMPLE_EMAIL_VALUE = 1;
    private static final int EMAIL_HEADER_VALUE = 2;
    private static final int EMAIL_HEADER_GREETINGS_VALUE = 3;
    private static final int EMAIL_HEADER_GREETINGS_SIGNATURE_VALUE = 4;
    private static final int QUIT_VALUE = 0;

    //other messages
    private static final String WRONG_INPUT_MESSAGE = "Podano niewłaściwą wartość";

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            printTitles();
            try {
                int input = scanner.nextInt();
                if (input == QUIT_VALUE) {
                    break;
                } else {
                    handleInput(input);
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_INPUT_MESSAGE);
                scanner.next();
            }
        }
        scanner.close();
    }

    /**
     * Prints "GUI" part
     */
    private static void printTitles() {
        System.out.println(PROGRAM_TITLE);
        System.out.println(SIMPLE_EMAIL_OPTION);
        System.out.println(EMAIL_HEADER_OPTION);
        System.out.println(EMAIL_HEADER_GREETINGS_OPTION);
        System.out.println(EMAIL_HEADER_GREETINGS_SIGNATURE_OPTION);
        System.out.println(QUIT_OPTION);
    }

    /**
     * Handles user's input
     *
     * @param input input number
     */
    private static void handleInput(int input) {
        Email email = new Email();
        switch (input) {
            case SIMPLE_EMAIL_VALUE:
                showSimpleValue(email);
                break;
            case EMAIL_HEADER_VALUE:
                showEmailHeaderValue(email);
                break;
            case EMAIL_HEADER_GREETINGS_VALUE:
                showEmailHeaderGreetingsValue(email);
                break;
            case EMAIL_HEADER_GREETINGS_SIGNATURE_VALUE:
                showEmailFullValue(email);
                break;
            default:
                System.out.println(WRONG_INPUT_MESSAGE);
                break;
        }
    }

    /**
     * Shows simple email text (with no decorators applied)
     *
     * @param email given {@link Email} object
     */
    private static void showSimpleValue(Email email) {
        System.out.println();
        System.out.println(email.getEmailText());
        System.out.println();
    }

    /**
     * Shows email text decorated by {@link HeaderEmailDecorator}
     *
     * @param email given {@link Email} object
     */
    private static void showEmailHeaderValue(Email email) {
        email = new HeaderEmailDecorator(email);
        System.out.println();
        System.out.println(email.getEmailText());
        System.out.println();
    }

    /**
     * Shows email text decorated by {@link HeaderEmailDecorator} and {@link GreetingsEmailDecorator}
     *
     * @param email given {@link Email} object
     */
    private static void showEmailHeaderGreetingsValue(Email email) {
        email = new HeaderEmailDecorator(email);
        email = new GreetingsEmailDecorator(email);
        System.out.println();
        System.out.println(email.getEmailText());
        System.out.println();
    }

    /**
     * Shows email text decorated by {@link HeaderEmailDecorator}, {@link GreetingsEmailDecorator}
     * and {@link SignatureEmailDecorator}
     *
     * @param email given {@link Email} object
     */
    private static void showEmailFullValue(Email email) {
        email = new HeaderEmailDecorator(email);
        email = new GreetingsEmailDecorator(email);
        email = new SignatureEmailDecorator(email);
        System.out.println();
        System.out.println(email.getEmailText());
        System.out.println();
    }
}
