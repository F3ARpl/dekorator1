package decorators;

import models.Email;

/**
 * Email greetings decorator class
 */
public class GreetingsEmailDecorator extends AbstractEmailDecorator {

    @SuppressWarnings("FieldCanBeLocal")
    private final String EMAIL_SAMPLE_GREETINGS = "\nŻyczę wszystkiego co najlepsze.";

    public GreetingsEmailDecorator(Email email) {
        super(email);
    }

    @Override
    public String getEmailText() {
        return buildEmailWithGreetings(getEmail().getEmailText());
    }

    /**
     * Adds greetings to given email
     *
     * @param email given email text
     * @return email with greetings
     */
    private String buildEmailWithGreetings(String email) {
        return email + EMAIL_SAMPLE_GREETINGS;
    }
}
